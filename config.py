from flask.ext.sqlalchemy import SQLAlchemy
from vandals_t1 import app

app.config['SQLALCHEMY_DATABASE_URI'] = 'sqlite:///vandals.db'
db = SQLAlchemy(app)