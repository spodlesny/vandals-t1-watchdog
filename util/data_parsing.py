from flask import json

def is_json(json_data):
    try:
        json.loads(json_data)
        return True
    except Exception:
        return False
