# coding=utf-8
import urllib2
from datetime import datetime, timedelta
import time
from flask import json
from vandals_t1 import db
from vandals_t1.models import Players


def get_last_round(url):
    response = urllib2.urlopen(url)
    json_object = response.read()
    response_dict = json.loads(json_object)

    if response_dict["status"] == "Active":
        last_round  = response_dict["round"]-1
        return str(last_round)
    else:
        last_round = response_dict["round"]
        return str(last_round)

def round_info(url, round_num):
    while True:
        response = urllib2.urlopen(url+round_num)
        json_object = response.read()
        try:
            #print json_object
            response_dict = json.loads(json_object) #20-09-2015 20:58:14:972
            last_hit = datetime.strptime(response_dict[0]["time"], "%d-%m-%Y %H:%M:%S:%f").replace(second=0, microsecond=0)
            first_hit = datetime.strptime(response_dict[-1]["time"], "%d-%m-%Y %H:%M:%S:%f").replace(second=0, microsecond=0)
            break
        except KeyError:
            return {"error":"Round do not exist"}
            break
        except UnicodeDecodeError:
            print "restart"
            time.sleep(1)
    rozdiel = str(last_hit-first_hit)

    if rozdiel == "2:00:00":
        ended = True
    else:
        ended = False

    return {"ended":ended, "start" :first_hit, "end":last_hit}

def return_our_players(url, round_num, mu_id = 271):
        response = urllib2.urlopen(url+round_num)
        json_object = response.read()

        while True:
            try:
                response_dict = json.loads(json_object)
                break
            except (ValueError, UnicodeDecodeError):
                time.sleep(1)
                response = urllib2.urlopen(url+round_num)
                json_object = response.read()

        players = []
        for x in range(len(response_dict)):
            try:
                if response_dict[x]['militaryUnit'] == mu_id:#problem :)
                    players.append(response_dict[x])
            except KeyError:
                pass
        return players

def get_player_name(id, url = "http://secura.e-sim.org/apiCitizenById.html?id="):
    response = urllib2.urlopen(url+str(id))
    json_object = response.read()
    response_dict = json.loads(json_object)

    login_name = response_dict["login"]
    return login_name

def return_players_stats(url, round_num):
    fucking_round_info = round_info(url, round_num)

    if "error" in fucking_round_info:
        return json.dumps({"error":"Round do not exist"})

    end_of_battle = fucking_round_info["end"]


    dict = {}
    round_data = return_our_players(url, round_num)

    #if fucking_round_info["ended"] is False: #get warning is round still not ended
    #    return json.dumps({"Round not ended yet"})

    for player_data in round_data:
        if Players.query.filter_by(id=player_data["citizenId"]).first() is None:
            nick = get_player_name(player_data["citizenId"])
            db.session.add(Players(nick=nick, id=player_data["citizenId"]))
            db.session.commit()
        else:
            nick_data = Players.query.filter_by(id=player_data["citizenId"]).first()
            nick = nick_data.nick

        weap_t1 = 0 #must be 0 because not is not posible to sum
        weap_rest = 0
        damage_t1 = 0
        damage_rest = 0

        time_of_hit = datetime.strptime(player_data["time"], "%d-%m-%Y %H:%M:%S:%f").replace(second=0, microsecond=0)

        if (end_of_battle - time_of_hit) <= timedelta(minutes=1):
            if player_data["berserk"] == True:
                weap_t1 = 5
            else:
                weap_t1 = 1
            damage_t1 = player_data["damage"]
        else:
            if player_data["berserk"] == True:
                weap_rest = 5
            else:
                weap_rest = 1
            damage_rest = player_data["damage"]

        if nick in dict:
            dict[nick]["weap_t1"] += weap_t1
            dict[nick]["weap_rest"] += weap_rest
            dict[nick]["damage_t1"] += damage_t1
            dict[nick]["damage_rest"] += damage_rest
        else:
            dict[nick] = {"weap_t1":weap_t1, "damage_t1":damage_t1, "weap_rest":weap_rest, "damage_rest":damage_rest}

    if not dict:
        return str('{"error":"No one fight!"}')
    else:
        return dict

