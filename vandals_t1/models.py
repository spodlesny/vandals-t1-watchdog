from vandals_t1 import db


class Players(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    nick = db.Column(db.String(256), unique=True)

    def __init__(self, id, nick):
        self.nick = nick
        self.id = id

