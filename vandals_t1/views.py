# coding=utf-8
from flask import json
from vandals_t1 import app
from vandals_t1.battle import get_last_round, return_players_stats



@app.route('/')
@app.route('/battle/')
def help_page():
    return "For last round use:<br><b>http://website.sk/battle/id</b><br>" \
           "For specified round use:<br><b>http://website.sk/battle/id/round/num</b><br><br>" \
           "If you want to fork this project visit: <a href=https://bitbucket.org/spodlesny/vandals-t1-watchdog>https://bitbucket.org/spodlesny/vandals-t1-watchdog</a>"

@app.route('/battle/<id>/') #get back
def battle(id):
    print id
    round_num = get_last_round("https://www.cscpro.org/secura/battle/"+str(id)+".json")
    stats = {}
    for x in range(1, int(round_num)+1):
        if isinstance(return_players_stats("http://secura.e-sim.org/apiFights.html?battleId="+str(id)+"&roundId=", str(x)), str):
            stats[x] = {}
        else:
            stats[x] = return_players_stats("http://secura.e-sim.org/apiFights.html?battleId="+str(id)+"&roundId=", str(x))

    final_stats = {}
    for x in stats:
        for player in stats[x]:
            if player in final_stats:
              final_stats[player]["weap_t1"] += stats[x][player]["weap_t1"]
              final_stats[player]["weap_rest"] += stats[x][player]["weap_rest"]
              final_stats[player]["damage_t1"] += stats[x][player]["damage_t1"]
              final_stats[player]["damage_rest"] += stats[x][player]["damage_rest"]
            else:
                final_stats[player] = {"weap_t1":stats[x][player]["weap_t1"], "damage_t1":stats[x][player]["damage_t1"], "weap_rest":stats[x][player]["weap_rest"], "damage_rest":stats[x][player]["damage_rest"]}

    json_output = json.dumps(final_stats)
    return json_output

@app.route('/battle/<id>/round/<round>/')
def battle_spec_round(id, round):
    url = "http://secura.e-sim.org/apiFights.html?battleId="+id+"&roundId="
    round = str(round)
    json_output = json.dumps(return_players_stats(url, round))
    return json_output


