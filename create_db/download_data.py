import os
import random
import timeit
import urllib2

import time
from flask import json

from util.data_parsing import is_json

active__battles = []


#This function return round number or None if error occured
def find_last_round(battle_id):
    for round_num in xrange(16, 0, -1):
        while True:
            response = urllib2.urlopen("http://secura.e-sim.org/apiFights.html?battleId="+str(battle_id)+"&roundId="+str(round_num))
            json_data = response.read()
            if is_json(json_data) == True: #will detect redirect if there is too many request
                break
            else:
                print "\t\tSleeping in find_last_round()"
                time.sleep(random.random())

        try:
            dict = json.loads(json_data)
            dict["error"]
        except Exception:
            return round_num

def battle_exist(battle_id):
    while True:
        response = urllib2.urlopen("http://secura.e-sim.org/apiFights.html?battleId="+str(battle_id)+"&roundId=1")
        json_data = response.read()
        if is_json(json_data) == True: #will detect redirect if there is too many request
            break
        else:
            print "\t\tSleeping in battle_exist()"
            time.sleep(random.random())
    try:
        dict = json.loads(json_data)
        dict["error"]
        return False
    except Exception:
        return True

#Function create folder for every battle in data/
#This function do not check if existing folder contains all files!
def download_battle(battle_id):
    try:
        os.makedirs("data/"+str(battle_id))
    except OSError:
        print "Ignorring - folder exist"
        return False

    print "\tCopying data from battle: "+str(battle_id)
    for actual_round in xrange(1, find_last_round(battle_id)+1):
        while True:
            response = urllib2.urlopen("http://secura.e-sim.org/apiFights.html?battleId="+str(battle_id)+"&roundId="+str(actual_round))
            json_data = response.read()
            if is_json(json_data) == True: #will detect redirect if there is too many request
                break
            else:
                print "\t\tSleeping in download_battle()"
                time.sleep(random.random())

        f = open("data/"+str(battle_id)+"/"+str(actual_round), 'w')
        f.write(json_data)
        f.close()
    return True

def find_active_battles():
    while True:
        print "Trying to reach unofficial API"
        response = urllib2.urlopen("https://www.cscpro.org/secura/battles.json")
        print "Opening active battles"
        json_data = response.read()
        if is_json(json_data) == True: #will detect redirect if there is too many request
            break
        else:
            print "\t\tSleeping in find_activebattles()"
            time.sleep(random.random())

    dict = json.loads(json_data)


    for x in range(0, len(dict["battles"])):
        active__battles.append(dict["battles"][x]["id"])

#Someone can call this main()
start = timeit.default_timer()
print "Let's start"

find_active_battles()
#if e-sim will exist and this script will be still funtional after one million battles, contact me, and we can celebrate my coding skills or admins inactivity :)
for x in range(26859, 1000000):
    if battle_exist(x) == True:
        print "Discovering battle "+str(x)
        if x not in active__battles:
            download_battle(x)
        else:
            print "\tIgnorring - battle not ended yet"
    else:
        stop = timeit.default_timer()
        print "\nThat's all. It takes only: "+str(stop - start)+" seconds"
        break

